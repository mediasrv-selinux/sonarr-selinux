include /usr/share/selinux/devel/Makefile

define assign-sonarr-ports
echo "Assigning Sonarr port..."
semanage port -m -t sonarr_port_t -p tcp 8989
semanage port -m -t sonarr_analytics_port_t -p tcp 10000
endef

define remove-sonarr-ports
echo "Removing Sonarr port..."
semanage port -d -t sonarr_port_t -p tcp 8989
semanage port -d -t sonarr_analytics_port_t -p tcp 10000
endef

define relabel-sonarr-files
echo "Relabeling files..."
restorecon -DR /var/lib/sonarr
restorecon -DR /opt/Sonarr
endef

define force-relabel-sonarr-files
echo "Relabeling files..."
restorecon -R /var/lib/sonarr/
restorecon -R /opt/Sonarr/
endef

.PHONY: install uninstall update

install:
	semodule -v -i sonarr.pp
	$(assign-sonarr-ports)
	$(relabel-sonarr-files)

uninstall:
	$(remove-sonarr-ports)
	semodule -v -r sonarr
	$(relabel-sonarr-files)	

update:
	semodule -v -i sonarr.pp
	$(force-relabel-sonarr-files)
