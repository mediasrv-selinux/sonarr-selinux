# SElinux module for Sonarr v3

This is an SELinux policy module that defines a domain to confine Sonarr v3
and associated processes. By default, Sonarr runs in the
`unconfined_service_t` domain. This gives Sonarr far more access to the system
than it needs to do its job. Installing this policy module will confine Sonarr
in its own domain with a tailored set of access controls.

This policy module should not interfere with Sonarrs's normal operation except
where noted (i.e Web UI file browser). If you find something is not working
correctly please open an issue. However all booleans default to **off** and
you will need to enable the ones matching your chosen download clients and search
clients or sonarr will not be able to function.

The policy is currently designed to work with the [plex](https://gitlab.com/mediasrv-selinux/plex-selinux),
[NZBget](https://gitlab.com/mediasrv-selinux/nzbget-selinux), [jackett](https://gitlab.com/mediasrv-selinux/jackett-selinux) and [transmission](https://gitlab.com/mediasrv-selinux/transmission-selinux) policies. It is modular using conditional statements within the policy so it has
no dependencies on any other module. Additional download clients and search clients may be added easily.

Bug reports and patches are welcome.

### Support Status

Supported Platforms: CentOS 7-8, Fedora 29-32

Supported Sonarr Version: 3.0.x.x

### Current Release Notes

The included sonarr.service file ensures that Sonarr is run under the proper SELinux
domain. You may use it as is or change it to suit your system.

The included sonarr.service for systemd also moves Sonarrs tmp dir from /tmp to /opt/Sonarr/tmp.
If you are not installing from RPM this directory must be created.

Automatic update (self update) is not supported when using this policy, if SELinux is
enforcing it *will* prevent updates. Using the RPM package or manually updating is required.

The policy requires that Sonarr's home directory be `/var/lib/sonarr` instead
of the traditional location in /home. This is in line with Red Hats file system
standards. You may simply edit the user and move /home/Sonarr/.config to the new location.
Installing the module will re-label the files correctly.

## Building

### Requirements

In order to build and install this policy module you will need the following
packages installed:

* make
* selinux-policy
* selinux-policy-devel
* policycoreutils-python for Red Hat/CentOS <= 7 and Fedora <=31
* python3-policycoreutils for Red Hat CentOS 8+ and Fedora 32+


### Build

	$ make

### Install

Installs the policy package and relabels Sonarr's files to new file
contexts.

(as root)

	# make install

### Update

Updates the policy without modifying the SElinux port contexts to save time.

    # make update

### Uninstall

Uninstalls the policy module from the system and relabels Sonarr's
files to the default file contexts.

(as root)

	# make uninstall

## Policy Details

### Domain

SELinux uses domains to label processes. The domain of a confined process
defines what access controls the process is subjected to.

The security context of a process can be inspected by adding the `-Z` option to
the `ps` command

	$ ps -eZ | grep sonarr_t
	system_u:system_r:sonarr_t:s0    474378 ?      00:02:57 mono

This policy module defines the domain `sonarr_t` for Sonarr processes.

### File Contexts

SELinux requires that files be labeled with a security context so it knows what
access controls to apply. This security context is usually stored in an extended
attribute on the file.

The security context of a file can be inspected by using the `-Z` option with
the `ls` command.

	$ ls -Z /opt/Sonarr
	system_u:object_r:sonarr_exec_t:s0 Sonarr.exe

A file's security context can be changed temporarily using `chcon`. These
changes will not survive a relabeling.

	$ chcon -R /usr/share/my_media_library

A file's security context can be changed permanently by using `semanage fcontext`
to define the default security context for a given set of files and then using
`restorecon` to apply those labels.

	$ semanage fcontext -a -t sonarr_var_lib_t "/opt/Sonarr(/.*)?"
	$ restorecon -v -R /opt/Sonarr

This policy module defines the following file contexts:

##### sonarr\_etc\_t

sonarr configuration files.
<br />
<br />

##### sonarr\_initrc\_exec\_t

sonarr init.d scripts or systemd unit files.
<br />
<br />

##### sonarr\_exec\_t

sonarr executables. Files labeled with this context will create
processes in the `sonarr_t` domain when called from certain domains
(notably `init_t` and `initrc_t`). Generally a transition to `sonarr_t` will not
occur if the calling process is `unconfined_t` (the standard user context in
the targeted policy). This means if a user calls the executable from a shell
the resulting process **will not** be confined.
<br />
<br />

##### sonarr\_log\_t

Sonarr log files. Using this context will allow common logging utils such
as logrotate to access and manipulate these files. Processes confined by `sonarr_t`
can manage files and directories in this domain.
<br />
<br />

##### sonarr\_var\_lib\_t

sonarr `/var/lib` files. Processes confined by `sonarr_t` can manage
files and directories in this domain.
<br />
<br />

##### sonarr\_tmp\_t and sonarr\_tmpfs\_t

sonarr temporary files and objects. Processes confined by `sonarr_t`
can manage files and directories in this domain.
<br />
<br />

##### Executable contexts

Processes confined by `sonarr_t` have permission to execute files in the
following security contexts:

* `sonarr_exec_t`
* `bin_t`

Processes created from executables in these contexts will be confined by `sonarr_t`.
<br />
<br />

##### Shared contexts

Processes confined by `sonarr_t` have read access to the following
shared file contexts:

* `public_content_t`
* `public_content_rw_t`

This is useful if you want label media files to be readable by processes in
domains other than `sonarr_t` (Apache, NFS, Samba, FTP, etc)
<br />
<br />

### Networking

Processes confined by `sonarr_t` have the following network capabilities:

* May bind TCP sockets to `sonarr_port_t`
* May connect TCP sockets to `http_port_t`
* May connect TCP sockets to `port_t`
* May make DNS queries (TCP and UDP).
* May send log messages to syslog/journald.

### Booleans

SELinux booleans can be used to adjust the access controls enforced for
`sonarr_t`. Default settings (off) result in tightest security. Enabling a boolean
will loosen access controls and allow `sonarr_t` confined processes more access
to the system.

The status of booleans can be inspected using `getsebool`

	$ getsebool -a | grep sonarr
	sonarr_access_all_ro --> off
	sonarr_access_all_rw --> off
	sonarr_access_home_dirs_rw --> off
	sonarr_access_sys_cert_ro --> off
	sonarr_anon_write --> off
	sonarr_mediasrv_write --> on
	sonarr_send_email --> off
	sonarr\_send\_analytics --> off


A boolean can be turned on or off using `setsebool`

(as root)

`# setsebool allow_sonarr_list_all_dirs on`

A `-P` option can be passed to `setsebool` that makes the setting persist
across reboots.

This policy modules defines the following booleans:

##### sonarr\_access\_all\_ro

Allow processes contained by `sonarr_t` to read all files and directories.
<br />
<br />
Defaults to **off**. Should not be needed or used.
<br />
<br />

##### sonarr\_access\_all\_rw

Allow processes contained by `sonarr_t` to manage (read, write, create, delete)
all files and directories.
<br />
<br />
Defaults to **off** and should remain off. If you need this you are doing it wrong!
<br />
<br />

##### sonarr\_anon\_write

Allow processes contained by `sonarr_t` to manage (read, write, create, delete)
files and directories labeled with the `public_content_rw_t` context.
<br />
<br />
Defaults to **off**
<br />
<br />

##### sonarr\_access\_home\_dirs\_rw

Allow processes contained by `sonarr_t` to manage (read, write, create, delete)
files and directories in user home directories.
<br />
<br />
Defaults to **off**
<br />
<br />

##### sonarr\_sys\_certs\_ro

Allows processes contained by `sonarr_t` to read cert\_t labeled files, normally this
is the system wide TLS certificates stored in /etc/pki/
<br />
<br />
This defaults to **on** so that the SSL certs for sonarr may be stored in the default locations.
<br />
<br />

##### sonarr\_send\_email

Allows processes contained by `sonarr_t` to connect to `smtp_port_t` to send email notifications.
<br />
<br />
This defaults to **off**.
<br />
<br />

##### sonarr\_send\_analytics

Sonarr may optionally send analytcs to the devs via a third-part site. Enabling this
boolean allows `sonarr_t` to access `sonarr_analytics_port_t`.
<br />
<br />
This defaults to **off**.

### Optional Booleans

These values are only present if the applications and selinux polices from them are installed
on the system.

##### sonarr\_use\_mediasrv

Allows processes contained by `sonarr_t` to use the Mediaserver Framework shared contexts. This
allows for a shared file context between multiple media downloading and sharing applications
without using the `public_content_rw_t` context.
<br />
<br />
This defaults to **off**.

If enabled `sonarr_t` will have access to the following shared contexts:

* `mediasrv_content_ro_t`
* `mediasrv_content_rw_t`
<br />
<br />

##### sonarr\_connect\_jackett

Allows processes contained by `sonarr_t` to connect to `jackett_port_t` and use the
jackett indexing/search service.
<br />
<br />
This defaults to **off**.
<br />
<br />

##### sonarr\_connect\_nzbget

Allows processes contained by `sonarr_t` to connect to `nzbget_port_t` and use the
nzbget unsenet download service.
<br />
<br />
This defaults to **off**.

If enabled `sonarr_t` will have access to the following shared context:

* `nzbget_content_rw_t`
<br />
<br />

##### sonarr\_connect\_transmission

Allows processes contained by `sonarr_t` to connect to `transmissiont_port_t` and use the
transmission bittorrent download service.
<br />
<br />
This defaults to **off**.

If enabled `sonarr_t` will have access to the following shared context:

* `transmission_content_rw_t`
<br />
<br />



