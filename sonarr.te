policy_module(sonarr, 1.0.2)

#
# Access policies
#

# The target domain
type sonarr_t;

# Main entry point and executables
type sonarr_exec_t;

# Daemon transition
init_daemon_domain(sonarr_t, sonarr_exec_t)

# Allow executing own executables
can_exec(sonarr_t, sonarr_exec_t)

# Define init script domain and transition
type sonarr_initrc_exec_t;
init_script_file(sonarr_initrc_exec_t)

# Allow access to shm
allow sonarr_t self:shm create_shm_perms;

# Create a domain for Sonarr config file and
# allow sonarr_t to read it .
type sonarr_etc_t;
files_config_file(sonarr_etc_t)
manage_dirs_pattern(sonarr_t, sonarr_etc_t, sonarr_etc_t)
manage_files_pattern(sonarr_t, sonarr_etc_t, sonarr_etc_t)
mmap_read_files_pattern(sonarr_t, sonarr_etc_t, sonarr_etc_t)


# Create a domain for Sonarr variable files
# and allow sonarr_t to manage them.
type sonarr_var_lib_t;
files_type(sonarr_var_lib_t)
files_search_var_lib(sonarr_t)
manage_dirs_pattern(sonarr_t, sonarr_var_lib_t, sonarr_var_lib_t)
manage_files_pattern(sonarr_t, sonarr_var_lib_t, sonarr_var_lib_t)
mmap_read_files_pattern(sonarr_t, sonarr_var_lib_t, sonarr_var_lib_t)
manage_lnk_files_pattern(sonarr_t, sonarr_var_lib_t, sonarr_var_lib_t)
files_var_lib_filetrans(sonarr_t, sonarr_var_lib_t, { dir })

# Create a domain for Sonarr log files
# and allow sonarr_t to manage them.
type sonarr_log_t;
logging_log_file(sonarr_log_t)
manage_dirs_pattern(sonarr_t, sonarr_log_t, sonarr_log_t)
manage_files_pattern(sonarr_t, sonarr_log_t, sonarr_log_t)
manage_lnk_files_pattern(sonarr_t, sonarr_log_t, sonarr_log_t)
logging_log_filetrans(sonarr_t, sonarr_log_t, { dir file lnk_file })


# Create a domain for Sonarr tmp files
# and allow sonarr_t to manage them.
type sonarr_tmp_t;
files_tmp_file(sonarr_tmp_t)
manage_dirs_pattern(sonarr_t, sonarr_tmp_t, sonarr_tmp_t)
manage_files_pattern(sonarr_t, sonarr_tmp_t, sonarr_tmp_t)
manage_lnk_files_pattern(sonarr_t, sonarr_tmp_t, sonarr_tmp_t)
files_tmp_filetrans(sonarr_t, sonarr_tmp_t, { dir file lnk_file })

# Create a domain for Sonarr tmpfs files
# and allow sonarr_t to manage them.
type sonarr_tmpfs_t;
files_tmpfs_file(sonarr_tmpfs_t)
manage_dirs_pattern(sonarr_t, sonarr_tmpfs_t, sonarr_tmpfs_t)
manage_files_pattern(sonarr_t, sonarr_tmpfs_t, sonarr_tmpfs_t)
mmap_read_files_pattern(sonarr_t, sonarr_tmpfs_t, sonarr_tmpfs_t)
manage_lnk_files_pattern(sonarr_t, sonarr_tmpfs_t, sonarr_tmpfs_t)
fs_tmpfs_filetrans(sonarr_t, sonarr_tmpfs_t, { dir file lnk_file })


# Define sonarr port
type sonarr_port_t;
corenet_port(sonarr_port_t)

allow sonarr_t sonarr_port_t:tcp_socket { name_bind name_connect };

# Define sonarr analytics port, only used if
# boolean is set, but always defined.
type sonarr_analytics_port_t;
corenet_port(sonarr_analytics_port_t)

sysnet_dns_name_resolve(sonarr_t)

#allow sonarr_t self:udp_socket { name_connect };
#allow sonarr_t self:udp_socket create_socket_perms;
allow sonarr_t self:tcp_socket create_stream_socket_perms;
allow sonarr_t self:unix_dgram_socket { create_socket_perms sendto };
allow sonarr_t self:netlink_route_socket r_netlink_socket_perms;

# Allow Sonarr to access all interfaces and
# nodes (IP Addresses)
corenet_tcp_sendrecv_all_if(sonarr_t)
corenet_udp_sendrecv_all_if(sonarr_t)
corenet_tcp_sendrecv_all_nodes(sonarr_t)
corenet_udp_sendrecv_all_nodes(sonarr_t)

# Allow access to http/https port
corenet_tcp_connect_http_port(sonarr_t)


#
# System and Process stuff
#

#allow sonarr_t self:process { getcap setcap signal_perms setsched setexec execmem };
allow sonarr_t self:process { setsched signal_perms execmem setsched };
fs_rw_anon_inodefs_files(sonarr_t)
fs_getattr_all_fs(sonarr_t)
init_read_state(sonarr_t)

# Allow reading system locale
miscfiles_read_localization(sonarr_t)

# Allow reading random devices
dev_read_rand(sonarr_t)
dev_read_urand(sonarr_t)

# Allow executing /usr/bin commands. Sonarr needs this to
# invoke mono.
corecmd_exec_bin(sonarr_t)

# Allow executing bin_t files for mono
allow sonarr_t bin_t:file { entrypoint execute };

# Allow configuring the dynamic linker
libs_exec_ldconfig(sonarr_t)

# Allow reading system state
kernel_read_system_state(sonarr_t)
kernel_read_network_state(sonarr_t)
kernel_read_fs_sysctls(sonarr_t)
dev_read_sysfs(sonarr_t)

# Allow reading system network configuration
sysnet_read_config(sonarr_t)

# Allow sonarr to use syslog/journald if configured to do so.
logging_send_syslog_msg(sonarr_t)

# Allow sonarr to use nsswitch
auth_use_nsswitch(sonarr_t)

# Allow Sonarr to list all mountpoints
# Silences complaints about autofs mountpoints
files_list_all_mountpoints(sonarr_t)

# This one is questionable. Sonarr attempts to list the contents of /proc/<pid> for
# number of <pids> that seem very unrelated. If access is denied sonarr will not start.
# I'm not sure why it does this, but in the mean time sonarr_t
# needs the ability to read other domains state.
domain_read_all_domains_state(sonarr_t)


## Optional policies

# Optional policy: Allow read/write of files labeled plex_content_rw_t without domain transition.

optional_policy(`
	require {
		type plex_t;
		type plex_port_t;
		type plex_content_rw_t;
		}

	# Allow sonarr to connect to plex port
	gen_tunable(sonarr_connect_plex, false)

	# Allow Sonarr to manage plex_*_content_t files
	gen_tunable(sonarr_manage_plex_content, false)

	tunable_policy(`sonarr_connect_plex',`
		allow sonarr_t plex_port_t:tcp_socket { name_bind name_connect };
	')

	tunable_policy(`sonarr_manage_plex_content',`
		allow sonarr_t plex_content_rw_t:file { read write create getattr setattr lock unlink link rename relabelto };
		allow sonarr_t plex_content_rw_t:dir { add_name remove_name reparent search rmdir open getattr setattr };
	')
')

# Use mediaserver framework policy if available
# Individual permissions used as conditionals cannot use attributes.
optional_policy(`
	require {
		type mediasrv_t;
		type mediasrv_content_ro_t;
		type mediasrv_content_rw_t;
		} #end require

	#Allow sonarr to read public mediasrv files by default.
	allow sonarr_t { mediasrv_content_ro_t mediasrv_content_rw_t }:dir { getattr search open read lock ioctl };
	allow sonarr_t { mediasrv_content_ro_t mediasrv_content_rw_t }:file { open getattr read ioctl lock };
	allow sonarr_t { mediasrv_content_ro_t mediasrv_content_rw_t }:lnk_file { getattr read };

	# Allow sonarr to manage mediasrv_content_rw_t labeled files and directories.
	# By default, sonarr can only read mediasrv_content_rw_t file and directories.
	gen_tunable(sonarr_mediasrv_write, false)

	tunable_policy(`sonarr_mediasrv_write',`
		allow sonarr_t mediasrv_content_rw_t:dir { create open getattr setattr read write link unlink rename search add_name remove_name reparent rmdir lock ioctl };
		allow sonarr_t mediasrv_content_rw_t:file { create open getattr setattr read write append rename link unlink ioctl lock };
		allow sonarr_t mediasrv_content_rw_t:lnk_file { create getattr setattr read write append rename link unlink ioctl lock };
	')
')

# Optional Policy: Use Jackett if available
optional_policy(`
	require {
		type jackett_t;
		type jackett_port_t;
		}

	# Allow sonarr to connect to jackett port
	gen_tunable(sonarr_connect_jackett, false)

	tunable_policy(`sonarr_connect_jackett',`
		allow sonarr_t jackett_port_t:tcp_socket { name_bind name_connect };
	')
')

# Optional Policy: Use nzbget if available
optional_policy(`
	require {
		type nzbget_t;
		type nzbget_port_t;
		type nzbget_content_rw_t;
		}

	# Allow sonarr to connect to nzbget port and manage downloaded files.
	gen_tunable(sonarr_connect_nzbget, false)

	tunable_policy(`sonarr_connect_nzbget',`
		allow sonarr_t nzbget_port_t:tcp_socket { name_bind name_connect };
		allow sonarr_t nzbget_content_rw_t:dir { create open getattr setattr read write link unlink rename search add_name remove_name reparent rmdir lock ioctl };
		allow sonarr_t nzbget_content_rw_t:file { create open getattr setattr read write append rename link unlink ioctl lock };
		allow sonarr_t nzbget_content_rw_t:lnk_file { create getattr setattr read write append rename link unlink ioctl lock };
	')
')

# Optional Policy: Use transmission if available
optional_policy(`
	require {
		type transmission_t;
		type transmission_port_t;
		type transmission_content_rw_t;
		}

	# Allow sonarr to connect to transmission-daemon port and manage downloaded files.
	gen_tunable(sonarr_connect_transmission, false)

	tunable_policy(`sonarr_connect_transmission',`
		allow sonarr_t transmission_port_t:tcp_socket { name_bind name_connect };
		allow sonarr_t transmission_content_rw_t:dir { create open getattr setattr read write link unlink rename search add_name remove_name reparent rmdir lock ioctl };
		allow sonarr_t transmission_content_rw_t:file { create open getattr setattr read write append rename link unlink ioctl lock };
		allow sonarr_t transmission_content_rw_t:lnk_file { create getattr setattr read write append rename link unlink ioctl lock };
	')
')

#
# Tunables
#

# Allows sonarr to read any file or directory on the system.
gen_tunable(sonarr_access_all_ro, false)

# Allows sonarr to manage any file or directory on the system.
gen_tunable(sonarr_access_all_rw, false)

# Allow sonarr to manage public_content_rw_t labeled files and directories.
# By default, sonarr can only read public_content_rw_t file and directories.
gen_tunable(sonarr_anon_write, false)

# Allows sonarr to list all directories. This enables the directory browser in
# the web ui to browse the filesystem.
#
# While this is disabled, directories cannot be browsed and paths must be typed
# in when adding libraries to sonarr.
#
# Attempting to use the directory browser ui with this disabled will
# generate a lot of AVC denials.
gen_tunable(sonarr_list_all_dirs, false)

# Allows sonarr to manage files in users home directories.
gen_tunable(sonarr_access_home_dirs_rw, false)

# Allows sonarr to read systemwide cert store in /etc/pki.
gen_tunable(sonarr_sys_cert_ro, false)

# Allow sonarr to connect to smtp port for sending email notifications
gen_tunable(sonarr_send_email, false)

# Allow Sonarr to send analytics to rapid 7s https://logentries.com/
gen_tunable(sonarr_send_analytics, false)

#
# Tunable policies
#
tunable_policy(`sonarr_access_all_ro',`
        fs_read_noxattr_fs_files(sonarr_t)
        files_list_non_auth_dirs(sonarr_t)
        files_read_non_auth_symlinks(sonarr_t)
        files_read_non_auth_files(sonarr_t)
')

tunable_policy(`sonarr_access_all_rw',`
        fs_read_noxattr_fs_files(sonarr_t)
        files_manage_non_auth_files(sonarr_t)
')

tunable_policy(`sonarr_anon_write',`
	miscfiles_manage_public_files(sonarr_t)
')

tunable_policy(`sonarr_list_all_dirs',`
    	files_list_non_auth_dirs(sonarr_t)
	files_read_non_auth_symlinks(sonarr_t)
	files_dontaudit_getattr_all_files(sonarr_t)
')

tunable_policy(`sonarr_access_home_dirs_rw',`
        userdom_manage_user_home_content_dirs(sonarr_t)
        userdom_manage_user_home_content_files(sonarr_t)
        userdom_manage_user_home_content_symlinks(sonarr_t)
')

tunable_policy(`sonarr_sys_cert_ro',`
	miscfiles_read_generic_certs(sonarr_t)
')

tunable_policy(`sonarr_send_email',`
	corenet_tcp_connect_smtp_port(sonarr_t)
')

tunable_policy(`sonarr_send_analytics',`
	allow sonarr_t sonarr_analytics_port_t:tcp_socket { name_bind name_connect };
')
